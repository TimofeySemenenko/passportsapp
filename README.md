# Cпособ решения задачи

  - Задача микросервиса - держать всегда актуальную "информацию" паспортов, действующих/не утерянных на основе данных ФМС, предполагаем, что у ФМС есть интерфейс взаимодействия `RESTful` или `Soap`. 
  
  - Т.к. база ФМС обновляется c какой-то частотой, для этого нужен микросервис, который бы занимался актуализацией данных в фоновом режиме, и разработка отдельного приложения, предоставляющего актуальные данные по RESTful API или UI интерфейсам, то есть разработка второго микросервиса. Он будет заниматься только отдачей клиентам данных из памяти.

# Архитектура сервиса

## Какой будет стек? 

  - Исходя из тех.задания, количество возможных инстансов не было указано. В связи с этим, при проектировании сервиса это не приоритетно и можно использовать технологии, которые помогли нам соблюсти условие, чтобы наше приложение, предоставляющее данные по действующем паспортам, возвращало бы информацию на запрос >=1ms. Для этого целесообразнее хранить актуальные данные паспортов в памяти, а не на диске сервера. В качестве хранилища возьмем базу данных `(Redis)` и продумаем сразу отказоустойчивый кластер, чтобы не по потеряли наши данные, используя только одну ноду, будем использовать min три ноды.
  
  - Нам также нужно соблюсти uptime 99%. 
  Приложение будет разработано на языке php, крайней на сегодняшней день стабильной версии 7.4, php медленный поэтому нам нужно, чтобы приложение было в uptime, даже если будет несколько тысяч одновременных коннектов. Для этого нам пригодится менеджер процессов `php-fpm`, создавая opcache скриптов. В условиях задачи не указано, сколько `RPS` планируется, поэтому закладываем дефолтную настройку php-fpm. Если будет несколько тысяч rps, то вертикальное масштабирование будет необходимо то, мы можем добавить дополнительно ядер на сервере приложения или сделать Xproxy после nginx и балансировать нагрузку на ноды.
  
   - Нам понадобится мониторинг, чтобы понимать, когда приложение не доступно или произошел `пожар` на продакшене, в качестве мониторинга возьмем Zabbix, а дэшборды будем подключать через Kibana. Нам нужно будет развернуть отказоустойчивый ELK кластер из min из 3 нод. 
  
## Как будут взаимодействовать сервисы между собой?
   
  - Первый микросервис получает данные по паспортам от второго микросервиса через RESTful API, который хранит данные по паспортам исходя из ТЗ. Далее проверяет все полученные данные с данными ФМС и кладет в Redis cluster. Предлагаю сделать для этого демон, который бы раз 24 часа обновлял данные в памяти и писал ошибки в logstash, чтобы вести далее в kibana дэшборды по ошибкам. 
  
  - Второй микросервис будет предоставлять RESTful API и UI интерфейс для получения актуальной информации по серии и номеру паспорта.
 
## Cхема взаимодействия сервисов
  
  [**Схема по ссылке** ](https://drive.google.com/file/d/1pjfceySPSeA6wCMuEZwXeTwo32Ve0wmI/view)

# Декомпозиция задачи и оценка по времени

 - Развертывание отказоустойчивого кластера Redis.
 
 Оценка по времени: 1d.
 - Развертывание отказоустойчивого кластера ELK
 
 Оценка по времени: 2d.
 
 - Разработка демона для первого микросервиса, который будет обращаться к локальному сервису паспортов по http, далее проверять актуальность каждого паспорта и класть в Redis. Написание 3 юнит-тестов, для методов проверки после получения данных от ФМС, получения данных от сервиса паспортов и метода который кладет данные в Redis.
 
 Оценка по времени: 1.5d.
 
 - Разработка второго микросервиса, API которое будет иметь два эндпоинта, первое -  для получения информации по конкретному паспорту методом post и второй - для получения списка всех актуальных паспортов методом get из Redis. Написание двух юнит-тестов для этих методов.
 
 Оценка по времени: 6h.
 
 - Написание конфигурации контейнеров для двух наших приложений + СI/CD конфигурацию
 
 Оценка по времени: 1.5d. 